package com.example.firebasetemplate;

import android.net.Uri;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

import com.bumptech.glide.Glide;
import com.example.firebasetemplate.databinding.FragmentEditProfileBinding;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.auth.UserProfileChangeRequest;
import com.google.firebase.storage.FirebaseStorage;

import java.util.UUID;

public class EditProfileFragment extends AppFragment {


    private FragmentEditProfileBinding binding;
    private Uri uriImagen;


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        return (binding = FragmentEditProfileBinding.inflate(inflater, container, false)).getRoot();
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        binding.updateProgressBar.setVisibility(View.GONE);

        binding.updateProfilePhoto.setOnClickListener(v -> seleccionarImagen());
        appViewModel.uriImagenSeleccionada.observe(getViewLifecycleOwner(), uri -> {
            if (uri != null) {
                Glide.with(this).load(uri).into(binding.updateProfilePhoto);
                uriImagen = uri;
            }
        });

        binding.btUpdateData.setOnClickListener(new View.OnClickListener() {
            FirebaseUser user = FirebaseAuth.getInstance().getCurrentUser();
            @Override
            public void onClick(View view) {
                binding.updateProgressBar.setVisibility(View.VISIBLE);
                if (!binding.nameUpdateText.getText().toString().isEmpty()) {
                            UserProfileChangeRequest profileUpdates = new UserProfileChangeRequest.Builder()
                                    .setDisplayName(binding.nameUpdateText.getText().toString())
                                    .build();

                            user.updateProfile(profileUpdates);
                }

                if (uriImagen != null) {
                    FirebaseStorage.getInstance()
                            .getReference("/images/" + UUID.randomUUID() + ".jpg")
                            .putFile(uriImagen)
                            .continueWithTask(task -> task.getResult().getStorage().getDownloadUrl())
                            .addOnSuccessListener(urlDescarga -> {
                                UserProfileChangeRequest profileUpdates = new UserProfileChangeRequest.Builder()
                                        .setPhotoUri(urlDescarga)
                                        .build();

                                user.updateProfile(profileUpdates);
                            });
                }
                binding.updateProgressBar.setVisibility(View.GONE);
                navController.popBackStack();
            }
        });
    }
}