package com.example.firebasetemplate;

import android.net.Uri;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

import com.bumptech.glide.Glide;
import com.example.firebasetemplate.databinding.FragmentRegisterBinding;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.auth.UserProfileChangeRequest;
import com.google.firebase.storage.FirebaseStorage;

import java.util.UUID;


public class RegisterFragment extends AppFragment {
    private FragmentRegisterBinding binding;
    private Uri uriImagen;


    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {

        return (binding = FragmentRegisterBinding.inflate(inflater, container, false)).getRoot();
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        binding.addProfilePhoto.setOnClickListener(v -> seleccionarImagen());

        appViewModel.uriImagenSeleccionada.observe(getViewLifecycleOwner(), uri -> {
            if (uri != null) {
                Glide.with(this).load(uri).into(binding.addProfilePhoto);
                uriImagen = uri;
            }
        });

        binding.createAccountButton.setOnClickListener(v -> {
            if (binding.passwordEditText.getText().toString().isEmpty()) {
                binding.passwordEditText.setError("Password required");
                return;
            }
            //para llamar a todos los metodos de authenticacion -> FirebaseAuth.getInstance()
            FirebaseStorage.getInstance()
                    .getReference("/images/"+ UUID.randomUUID()+".jpg")
                    .putFile(uriImagen)
                    .continueWithTask(task -> task.getResult().getStorage().getDownloadUrl())
                    .addOnSuccessListener(urlDescarga -> {
                        FirebaseAuth.getInstance().createUserWithEmailAndPassword(
                                binding.emailEditText.getText().toString(),
                                binding.passwordEditText.getText().toString()
                        ).addOnCompleteListener(new OnCompleteListener<AuthResult>() {

                            //añadimos el addonCompleteListener para que al registrarse se nos loguee automàticamente
                            @Override
                            public void onComplete(@NonNull Task<AuthResult> task) {
                                if (task.isSuccessful()) {
                                    FirebaseUser user = FirebaseAuth.getInstance().getCurrentUser();

                                    UserProfileChangeRequest profileUpdates = new UserProfileChangeRequest.Builder()
                                            .setDisplayName(binding.nameEditText.getText().toString())
                                            .setPhotoUri(urlDescarga)
                                            .build();

                                    user.updateProfile(profileUpdates);

                                    navController.navigate(R.id.action_registerFragment_to_postHomeFragment);

                                } else {
                                    Log.w("FAIL", "createUserWithEmail:failure", task.getException());
                                    Toast.makeText(requireContext(), task.getException().getLocalizedMessage(),
                                            Toast.LENGTH_SHORT).show();

                                }
                            }
                        });
                    });


        });
    }


}